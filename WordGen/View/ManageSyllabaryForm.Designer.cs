﻿namespace WordGen.View {
	partial class ManageSyllabaryForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Exemple", 0);
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageSyllabaryForm));
			this.TSeditor = new System.Windows.Forms.ToolStrip();
			this.Bnew = new System.Windows.Forms.ToolStripButton();
			this.Bedit = new System.Windows.Forms.ToolStripButton();
			this.Bduplicate = new System.Windows.Forms.ToolStripButton();
			this.Bdelete = new System.Windows.Forms.ToolStripButton();
			this.Bimport = new System.Windows.Forms.ToolStripButton();
			this.DDexport = new System.Windows.Forms.ToolStripDropDownButton();
			this.Bexport = new System.Windows.Forms.ToolStripMenuItem();
			this.BexportAll = new System.Windows.Forms.ToolStripMenuItem();
			this.Bcancel = new System.Windows.Forms.Button();
			this.Bok = new System.Windows.Forms.Button();
			this.LVSyllabary = new System.Windows.Forms.ListView();
			this.ILbig = new System.Windows.Forms.ImageList(this.components);
			this.ILsmall = new System.Windows.Forms.ImageList(this.components);
			this.syllabaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.TSeditor.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.syllabaryBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// TSeditor
			// 
			this.TSeditor.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.TSeditor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Bnew,
            this.Bedit,
            this.Bduplicate,
            this.Bdelete,
            this.Bimport,
            this.DDexport});
			this.TSeditor.Location = new System.Drawing.Point(0, 0);
			this.TSeditor.Name = "TSeditor";
			this.TSeditor.Size = new System.Drawing.Size(508, 25);
			this.TSeditor.TabIndex = 0;
			this.TSeditor.Text = "toolStrip1";
			// 
			// Bnew
			// 
			this.Bnew.Image = global::WordGen.Properties.Resources.NewBuildDefinition_8952;
			this.Bnew.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Bnew.Name = "Bnew";
			this.Bnew.Size = new System.Drawing.Size(84, 22);
			this.Bnew.Text = "&Nouveau...";
			this.Bnew.Click += new System.EventHandler(this.Bnew_Click);
			// 
			// Bedit
			// 
			this.Bedit.Enabled = false;
			this.Bedit.Image = global::WordGen.Properties.Resources.PencilAngled_16xLG_color;
			this.Bedit.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Bedit.Name = "Bedit";
			this.Bedit.Size = new System.Drawing.Size(66, 22);
			this.Bedit.Text = "&Editer...";
			this.Bedit.Click += new System.EventHandler(this.Bedit_Click);
			// 
			// Bduplicate
			// 
			this.Bduplicate.Enabled = false;
			this.Bduplicate.Image = global::WordGen.Properties.Resources.Copy_6524;
			this.Bduplicate.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Bduplicate.Name = "Bduplicate";
			this.Bduplicate.Size = new System.Drawing.Size(79, 22);
			this.Bduplicate.Text = "&Dupliquer";
			this.Bduplicate.Click += new System.EventHandler(this.Bduplicate_Click);
			// 
			// Bdelete
			// 
			this.Bdelete.Enabled = false;
			this.Bdelete.Image = global::WordGen.Properties.Resources.action_Cancel_16xLG;
			this.Bdelete.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Bdelete.Name = "Bdelete";
			this.Bdelete.Size = new System.Drawing.Size(82, 22);
			this.Bdelete.Text = "Supprimer";
			this.Bdelete.Click += new System.EventHandler(this.Bdelete_Click);
			// 
			// Bimport
			// 
			this.Bimport.Image = global::WordGen.Properties.Resources.Open_6529;
			this.Bimport.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Bimport.Name = "Bimport";
			this.Bimport.Size = new System.Drawing.Size(82, 22);
			this.Bimport.Text = "I&mporter...";
			this.Bimport.Click += new System.EventHandler(this.Bimport_Click);
			// 
			// DDexport
			// 
			this.DDexport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Bexport,
            this.BexportAll});
			this.DDexport.Image = global::WordGen.Properties.Resources.save_16xLG;
			this.DDexport.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.DDexport.Name = "DDexport";
			this.DDexport.Size = new System.Drawing.Size(79, 22);
			this.DDexport.Text = "E&xporter";
			// 
			// Bexport
			// 
			this.Bexport.Enabled = false;
			this.Bexport.Image = global::WordGen.Properties.Resources.save_16xLG;
			this.Bexport.Name = "Bexport";
			this.Bexport.Size = new System.Drawing.Size(151, 22);
			this.Bexport.Text = "Exporter...";
			this.Bexport.Click += new System.EventHandler(this.Bexport_Click);
			// 
			// BexportAll
			// 
			this.BexportAll.Image = global::WordGen.Properties.Resources.Saveall_6518;
			this.BexportAll.Name = "BexportAll";
			this.BexportAll.Size = new System.Drawing.Size(151, 22);
			this.BexportAll.Text = "Exporter tout...";
			this.BexportAll.Click += new System.EventHandler(this.BexportAll_Click);
			// 
			// Bcancel
			// 
			this.Bcancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.Bcancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Bcancel.Image = global::WordGen.Properties.Resources.Close_16xLG;
			this.Bcancel.Location = new System.Drawing.Point(421, 268);
			this.Bcancel.Name = "Bcancel";
			this.Bcancel.Size = new System.Drawing.Size(75, 23);
			this.Bcancel.TabIndex = 13;
			this.Bcancel.Text = "&Fermer";
			this.Bcancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.Bcancel.UseVisualStyleBackColor = true;
			// 
			// Bok
			// 
			this.Bok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.Bok.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.Bok.Image = global::WordGen.Properties.Resources.StatusAnnotations_Complete_and_ok_16xLG;
			this.Bok.Location = new System.Drawing.Point(340, 268);
			this.Bok.Name = "Bok";
			this.Bok.Size = new System.Drawing.Size(75, 23);
			this.Bok.TabIndex = 12;
			this.Bok.Text = "Ch&oisir";
			this.Bok.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.Bok.UseVisualStyleBackColor = true;
			this.Bok.Visible = false;
			this.Bok.Click += new System.EventHandler(this.Bclose_Click);
			// 
			// LVSyllabary
			// 
			this.LVSyllabary.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
			this.LVSyllabary.LargeImageList = this.ILbig;
			this.LVSyllabary.Location = new System.Drawing.Point(12, 28);
			this.LVSyllabary.Name = "LVSyllabary";
			this.LVSyllabary.ShowGroups = false;
			this.LVSyllabary.Size = new System.Drawing.Size(484, 234);
			this.LVSyllabary.SmallImageList = this.ILsmall;
			this.LVSyllabary.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.LVSyllabary.TabIndex = 14;
			this.LVSyllabary.UseCompatibleStateImageBehavior = false;
			this.LVSyllabary.View = System.Windows.Forms.View.Tile;
			this.LVSyllabary.SelectedIndexChanged += new System.EventHandler(this.LVsyllabary_SelectedIndexChanged);
			this.LVSyllabary.DoubleClick += new System.EventHandler(this.LVSyllabary_DoubleClick);
			this.LVSyllabary.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LVSyllabary_KeyDown);
			// 
			// ILbig
			// 
			this.ILbig.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ILbig.ImageStream")));
			this.ILbig.TransparentColor = System.Drawing.Color.Transparent;
			this.ILbig.Images.SetKeyName(0, "RefactoringLog12810_32.png");
			this.ILbig.Images.SetKeyName(1, "ASCube_32xLG.png");
			// 
			// ILsmall
			// 
			this.ILsmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ILsmall.ImageStream")));
			this.ILsmall.TransparentColor = System.Drawing.Color.Transparent;
			this.ILsmall.Images.SetKeyName(0, "RefactoringLog_12810.png");
			this.ILsmall.Images.SetKeyName(1, "ASCube_16xLG.png");
			// 
			// syllabaryBindingSource
			// 
			this.syllabaryBindingSource.DataSource = typeof(WordGen.Model.Syllabary);
			// 
			// ManageSyllabaryForm
			// 
			this.AcceptButton = this.Bok;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.Bcancel;
			this.ClientSize = new System.Drawing.Size(508, 303);
			this.Controls.Add(this.LVSyllabary);
			this.Controls.Add(this.Bcancel);
			this.Controls.Add(this.Bok);
			this.Controls.Add(this.TSeditor);
			this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimizeBox = false;
			this.Name = "ManageSyllabaryForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Gérer les syllabaires";
			this.Load += new System.EventHandler(this.ManageSyllabaryForm_Load);
			this.TSeditor.ResumeLayout(false);
			this.TSeditor.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.syllabaryBindingSource)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip TSeditor;
		private System.Windows.Forms.ToolStripButton Bnew;
		private System.Windows.Forms.ToolStripButton Bedit;
		private System.Windows.Forms.ToolStripButton Bduplicate;
		private System.Windows.Forms.ToolStripButton Bdelete;
		private System.Windows.Forms.ToolStripButton Bimport;
		private System.Windows.Forms.ToolStripDropDownButton DDexport;
		private System.Windows.Forms.ToolStripMenuItem Bexport;
		private System.Windows.Forms.ToolStripMenuItem BexportAll;
		private System.Windows.Forms.Button Bcancel;
		private System.Windows.Forms.Button Bok;
		private System.Windows.Forms.ListView LVSyllabary;
		private System.Windows.Forms.ImageList ILsmall;
		private System.Windows.Forms.BindingSource syllabaryBindingSource;
		private System.Windows.Forms.ImageList ILbig;
	}
}