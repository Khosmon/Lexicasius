﻿using WordGen.Model;
using WordGen.View.LocalizationString;
using System;
using System.Text;
using System.Windows.Forms;


namespace WordGen.View {
	public partial class SyllabaryForm : Form {

		private Syllabary _mysillab;

		public Syllabary MySyllabaire {
			get { return _mysillab; }
		}

		// Default constructor (New)
		public SyllabaryForm() {
			InitializeComponent();
			_mysillab = new Syllabary();
			DDbeginSyllables.SelectedIndex = 0;
			DDendSyllables.SelectedIndex = 0;

		}

		// Constructor with Syllabary to load (Edit)
		public SyllabaryForm(Syllabary thisSyllabary) {
			InitializeComponent();
			_mysillab = thisSyllabary;
			TBsyllabaryName.Text = thisSyllabary.title;
			StringBuilder composer = new StringBuilder();

			foreach (var aCons in thisSyllabary.consonants) {
				composer.AppendLine(aCons);
			}
			TBconsonants.Text = composer.ToString();

			composer.Clear();
			foreach (var aVow in thisSyllabary.vowels) {
				composer.AppendLine(aVow);
			}
			TBvowels.Text = composer.ToString();

			composer.Clear();
			foreach (var aSyl in thisSyllabary.syllables) {
				composer.AppendLine(aSyl);
			}
			TBsyllables.Text = composer.ToString();

			DDbeginSyllables.SelectedIndex = (int)_mysillab.beforeSyllable;
			DDendSyllables.SelectedIndex = (int)_mysillab.afterSyllable;

		}

		private void Syllabaire_Load(object sender, EventArgs e) {
			TTsyllablesHelp.SetToolTip(LhelpBeginSyllable, Messages.SyllabaireForm_HelpBeginSyllable);
			TTsyllablesHelp.SetToolTip(LhelpEndSyllable, Messages.SyllabaireForm_HelpEndSyllable);
		}

		private bool form_validation() {
			bool everythingOK = true;
			if (String.IsNullOrEmpty(TBsyllabaryName.Text.Trim())) {
				everythingOK = false;
				EPsyllabary.SetError(TBsyllabaryName, Messages.SyllabaireForm_ErrorNoName);
			}

			if (String.IsNullOrEmpty(TBconsonants.Text.Trim()) && String.IsNullOrEmpty(TBvowels.Text.Trim()) && String.IsNullOrEmpty(TBsyllables.Text.Trim())) {
				everythingOK = false;
				EPsyllabary.SetError(Lsyllable, Messages.SyllabaireForm_ErrorNoUseElements);
			}

			string[] slipschars = { "\r\n" };

			if (everythingOK) {
				_mysillab.title = TBsyllabaryName.Text;
				_mysillab.consonants.Clear();
				_mysillab.vowels.Clear();
				_mysillab.syllables.Clear();
				_mysillab.consonants.AddRange(TBconsonants.Text.Split(slipschars, StringSplitOptions.RemoveEmptyEntries));
				_mysillab.syllables.AddRange(TBsyllables.Text.Split(slipschars, StringSplitOptions.RemoveEmptyEntries));
				_mysillab.vowels.AddRange(TBvowels.Text.Split(slipschars, StringSplitOptions.RemoveEmptyEntries));
				switch (DDbeginSyllables.SelectedIndex) {
					case 1:
						_mysillab.beforeSyllable = BeforeSyllable.AutoSelect;
						break;
					case 2:
						_mysillab.beforeSyllable = BeforeSyllable.AutoInsert;
						break;
					default:
						_mysillab.beforeSyllable = BeforeSyllable.AsIs;
						break;
				}

				switch (DDendSyllables.SelectedIndex) {
					case 1:
						_mysillab.afterSyllable = AfterSyllable.AutoInsert;
						break;
					default:
						_mysillab.afterSyllable = AfterSyllable.AsIs;
						break;
				}
			}
			return everythingOK;
		}

		private void BOk_Click(object sender, EventArgs e) {
			if (form_validation()) {
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}

		private void TBsyllabaryName_TextChanged(object sender, EventArgs e) {
			EPsyllabary.SetError(TBsyllabaryName, String.Empty);
		}

		private void BCancel_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.Cancel;
			this.Close();
			this.Dispose();
		}

		private void SyllabaireForm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e) {
			if (e.Modifiers == Keys.Alt && e.KeyCode == Keys.Enter) {
				e.Handled = true;
				e.SuppressKeyPress = true;
				if (form_validation()) {
					this.DialogResult = DialogResult.OK;
					this.Close();
				}
			}
		}


		private void TBsyllables_TextChanged(object sender, EventArgs e) {
			var useSyllables = !String.IsNullOrEmpty(TBsyllables.Text.Trim());
			DDendSyllables.Enabled = useSyllables;
			DDbeginSyllables.Enabled = useSyllables;
		}

		private void LhelpBeginSyllable_MouseHover(object sender, EventArgs e) {
			TTsyllablesHelp.ToolTipTitle = Messages.SyllabaireForm_HelpBeginSyllableTitle;
		}

		private void LhelpEndSyllable_MouseHover(object sender, EventArgs e) {
			TTsyllablesHelp.ToolTipTitle = Messages.SyllabaireForm_HelpEndSyllableTitle;
		}
	}
}
