﻿namespace WordGen.View
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.DDsyllbary = new System.Windows.Forms.ComboBox();
			this.Lsyllabaire = new System.Windows.Forms.Label();
			this.RBwordsLenght = new System.Windows.Forms.RadioButton();
			this.NUPwordsLenght = new System.Windows.Forms.NumericUpDown();
			this.RBcustom = new System.Windows.Forms.RadioButton();
			this.TBwordStructure = new System.Windows.Forms.TextBox();
			this.PCommand = new System.Windows.Forms.Panel();
			this.NUPnbWords = new System.Windows.Forms.NumericUpDown();
			this.LnbWords = new System.Windows.Forms.Label();
			this.Bgenerate = new System.Windows.Forms.Button();
			this.BchooseSyllabaire = new System.Windows.Forms.Button();
			this.GBsyntax = new System.Windows.Forms.GroupBox();
			this.LquickGuide = new System.Windows.Forms.Label();
			this.LsyntaxHelp = new System.Windows.Forms.Label();
			this.GBlenght = new System.Windows.Forms.GroupBox();
			this.LnbChars = new System.Windows.Forms.Label();
			this.LvowelPercent = new System.Windows.Forms.Label();
			this.NUPvowelPercent = new System.Windows.Forms.NumericUpDown();
			this.CBincludeSyllab = new System.Windows.Forms.CheckBox();
			this.LsyllablePercent = new System.Windows.Forms.Label();
			this.NUPsyllablePercent = new System.Windows.Forms.NumericUpDown();
			this.CBallowBeginSyllable = new System.Windows.Forms.CheckBox();
			this.LbeginSyllablePercent = new System.Windows.Forms.Label();
			this.NUPbeginSyllablePercent = new System.Windows.Forms.NumericUpDown();
			this.TThelp = new System.Windows.Forms.ToolTip(this.components);
			this.BSsyllabary = new System.Windows.Forms.BindingSource(this.components);
			this.EPmain = new System.Windows.Forms.ErrorProvider(this.components);
			((System.ComponentModel.ISupportInitialize)(this.NUPwordsLenght)).BeginInit();
			this.PCommand.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.NUPnbWords)).BeginInit();
			this.GBsyntax.SuspendLayout();
			this.GBlenght.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.NUPvowelPercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.NUPsyllablePercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.NUPbeginSyllablePercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BSsyllabary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.EPmain)).BeginInit();
			this.SuspendLayout();
			// 
			// DDsyllbary
			// 
			this.DDsyllbary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DDsyllbary.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.DDsyllbary.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.DDsyllbary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.DDsyllbary.FormattingEnabled = true;
			this.DDsyllbary.Location = new System.Drawing.Point(74, 12);
			this.DDsyllbary.Name = "DDsyllbary";
			this.DDsyllbary.Size = new System.Drawing.Size(207, 21);
			this.DDsyllbary.TabIndex = 1;
			this.DDsyllbary.SelectedIndexChanged += new System.EventHandler(this.DDSyllabaire_SelectedIndexChanged);
			// 
			// Lsyllabaire
			// 
			this.Lsyllabaire.AutoSize = true;
			this.Lsyllabaire.Location = new System.Drawing.Point(12, 15);
			this.Lsyllabaire.Name = "Lsyllabaire";
			this.Lsyllabaire.Size = new System.Drawing.Size(56, 13);
			this.Lsyllabaire.TabIndex = 0;
			this.Lsyllabaire.Text = "Syllabaire";
			// 
			// RBwordsLenght
			// 
			this.RBwordsLenght.AutoSize = true;
			this.RBwordsLenght.Checked = true;
			this.RBwordsLenght.Location = new System.Drawing.Point(16, 37);
			this.RBwordsLenght.Name = "RBwordsLenght";
			this.RBwordsLenght.Size = new System.Drawing.Size(124, 17);
			this.RBwordsLenght.TabIndex = 3;
			this.RBwordsLenght.TabStop = true;
			this.RBwordsLenght.Text = "Longueur des mots";
			this.RBwordsLenght.UseVisualStyleBackColor = true;
			this.RBwordsLenght.CheckedChanged += new System.EventHandler(this.RBwordsLenght_CheckedChanged);
			// 
			// NUPwordsLenght
			// 
			this.NUPwordsLenght.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.NUPwordsLenght.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.NUPwordsLenght.Location = new System.Drawing.Point(6, 21);
			this.NUPwordsLenght.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.NUPwordsLenght.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.NUPwordsLenght.Name = "NUPwordsLenght";
			this.NUPwordsLenght.Size = new System.Drawing.Size(47, 22);
			this.NUPwordsLenght.TabIndex = 0;
			this.NUPwordsLenght.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.NUPwordsLenght.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
			this.NUPwordsLenght.ValueChanged += new System.EventHandler(this.NUPwordsLenght_ValueChanged);
			// 
			// RBcustom
			// 
			this.RBcustom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.RBcustom.AutoSize = true;
			this.RBcustom.Location = new System.Drawing.Point(16, 224);
			this.RBcustom.Name = "RBcustom";
			this.RBcustom.Size = new System.Drawing.Size(139, 17);
			this.RBcustom.TabIndex = 5;
			this.RBcustom.TabStop = true;
			this.RBcustom.Text = "Syntaxe personnalisée";
			this.RBcustom.UseVisualStyleBackColor = true;
			this.RBcustom.CheckedChanged += new System.EventHandler(this.RBcustom_CheckedChanged);
			// 
			// TBwordStructure
			// 
			this.TBwordStructure.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TBwordStructure.Enabled = false;
			this.TBwordStructure.Location = new System.Drawing.Point(6, 21);
			this.TBwordStructure.Name = "TBwordStructure";
			this.TBwordStructure.Size = new System.Drawing.Size(352, 22);
			this.TBwordStructure.TabIndex = 1;
			this.TBwordStructure.TextChanged += new System.EventHandler(this.TBwordStructure_TextChanged);
			// 
			// PCommand
			// 
			this.PCommand.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.PCommand.Controls.Add(this.NUPnbWords);
			this.PCommand.Controls.Add(this.LnbWords);
			this.PCommand.Controls.Add(this.Bgenerate);
			this.PCommand.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.PCommand.Location = new System.Drawing.Point(0, 354);
			this.PCommand.Name = "PCommand";
			this.PCommand.Size = new System.Drawing.Size(412, 48);
			this.PCommand.TabIndex = 7;
			// 
			// NUPnbWords
			// 
			this.NUPnbWords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.NUPnbWords.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.NUPnbWords.Location = new System.Drawing.Point(197, 14);
			this.NUPnbWords.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.NUPnbWords.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.NUPnbWords.Name = "NUPnbWords";
			this.NUPnbWords.Size = new System.Drawing.Size(109, 22);
			this.NUPnbWords.TabIndex = 1;
			this.NUPnbWords.ThousandsSeparator = true;
			this.NUPnbWords.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.NUPnbWords.ValueChanged += new System.EventHandler(this.NUPNbWords_ValueChanged);
			// 
			// LnbWords
			// 
			this.LnbWords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.LnbWords.AutoSize = true;
			this.LnbWords.Location = new System.Drawing.Point(9, 15);
			this.LnbWords.Name = "LnbWords";
			this.LnbWords.Size = new System.Drawing.Size(97, 13);
			this.LnbWords.TabIndex = 0;
			this.LnbWords.Text = "Nombres de mots";
			// 
			// Bgenerate
			// 
			this.Bgenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.Bgenerate.Image = global::WordGen.Properties.Resources.arrow_run_16xLG;
			this.Bgenerate.Location = new System.Drawing.Point(312, 13);
			this.Bgenerate.Name = "Bgenerate";
			this.Bgenerate.Size = new System.Drawing.Size(88, 23);
			this.Bgenerate.TabIndex = 2;
			this.Bgenerate.Text = "Générer";
			this.Bgenerate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.Bgenerate.UseVisualStyleBackColor = true;
			this.Bgenerate.Click += new System.EventHandler(this.BGenerate_Click);
			// 
			// BchooseSyllabaire
			// 
			this.BchooseSyllabaire.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BchooseSyllabaire.Image = global::WordGen.Properties.Resources.gear_16xLG;
			this.BchooseSyllabaire.Location = new System.Drawing.Point(312, 11);
			this.BchooseSyllabaire.Name = "BchooseSyllabaire";
			this.BchooseSyllabaire.Size = new System.Drawing.Size(88, 23);
			this.BchooseSyllabaire.TabIndex = 2;
			this.BchooseSyllabaire.Text = "Gérer...";
			this.BchooseSyllabaire.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.BchooseSyllabaire.UseVisualStyleBackColor = true;
			this.BchooseSyllabaire.Click += new System.EventHandler(this.BchooseSyllabaire_Click);
			// 
			// GBsyntax
			// 
			this.GBsyntax.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GBsyntax.Controls.Add(this.LquickGuide);
			this.GBsyntax.Controls.Add(this.LsyntaxHelp);
			this.GBsyntax.Controls.Add(this.TBwordStructure);
			this.GBsyntax.Location = new System.Drawing.Point(12, 226);
			this.GBsyntax.Name = "GBsyntax";
			this.GBsyntax.Size = new System.Drawing.Size(388, 122);
			this.GBsyntax.TabIndex = 6;
			this.GBsyntax.TabStop = false;
			// 
			// LquickGuide
			// 
			this.LquickGuide.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LquickGuide.Enabled = false;
			this.LquickGuide.Location = new System.Drawing.Point(6, 46);
			this.LquickGuide.Name = "LquickGuide";
			this.LquickGuide.Size = new System.Drawing.Size(376, 73);
			this.LquickGuide.TabIndex = 2;
			this.LquickGuide.Text = "Utilisez les caractères suivants pour composer une syntaxe personnalisée :\r\n٠  \'c" +
    "\' : une consonne au hasard, \r\n٠  \'v\' : une voyelle au hasard, \r\n٠  \'s\' : une syl" +
    "labe au hasard.\r\n";
			// 
			// LsyntaxHelp
			// 
			this.LsyntaxHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.LsyntaxHelp.Enabled = false;
			this.LsyntaxHelp.Image = global::WordGen.Properties.Resources.StatusAnnotations_Help_and_inconclusive_16xLG_color;
			this.LsyntaxHelp.Location = new System.Drawing.Point(366, 0);
			this.LsyntaxHelp.Name = "LsyntaxHelp";
			this.LsyntaxHelp.Size = new System.Drawing.Size(16, 16);
			this.LsyntaxHelp.TabIndex = 0;
			// 
			// GBlenght
			// 
			this.GBlenght.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GBlenght.Controls.Add(this.NUPwordsLenght);
			this.GBlenght.Controls.Add(this.LnbChars);
			this.GBlenght.Controls.Add(this.LvowelPercent);
			this.GBlenght.Controls.Add(this.NUPvowelPercent);
			this.GBlenght.Controls.Add(this.CBincludeSyllab);
			this.GBlenght.Controls.Add(this.LsyllablePercent);
			this.GBlenght.Controls.Add(this.NUPsyllablePercent);
			this.GBlenght.Controls.Add(this.CBallowBeginSyllable);
			this.GBlenght.Controls.Add(this.LbeginSyllablePercent);
			this.GBlenght.Controls.Add(this.NUPbeginSyllablePercent);
			this.GBlenght.Location = new System.Drawing.Point(12, 39);
			this.GBlenght.Name = "GBlenght";
			this.GBlenght.Size = new System.Drawing.Size(388, 181);
			this.GBlenght.TabIndex = 4;
			this.GBlenght.TabStop = false;
			// 
			// LnbChars
			// 
			this.LnbChars.AutoSize = true;
			this.LnbChars.Location = new System.Drawing.Point(59, 23);
			this.LnbChars.Name = "LnbChars";
			this.LnbChars.Size = new System.Drawing.Size(87, 13);
			this.LnbChars.TabIndex = 1;
			this.LnbChars.Text = "syllabe environ.";
			// 
			// LvowelPercent
			// 
			this.LvowelPercent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LvowelPercent.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.LvowelPercent.Image = global::WordGen.Properties.Resources.StatusAnnotations_Help_and_inconclusive_16xLG_color;
			this.LvowelPercent.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.LvowelPercent.Location = new System.Drawing.Point(5, 48);
			this.LvowelPercent.Name = "LvowelPercent";
			this.LvowelPercent.Size = new System.Drawing.Size(290, 16);
			this.LvowelPercent.TabIndex = 5;
			this.LvowelPercent.Text = "Probabilité de commencer par une voyelle (%)";
			this.LvowelPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// NUPvowelPercent
			// 
			this.NUPvowelPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.NUPvowelPercent.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.NUPvowelPercent.Location = new System.Drawing.Point(301, 48);
			this.NUPvowelPercent.Name = "NUPvowelPercent";
			this.NUPvowelPercent.Size = new System.Drawing.Size(61, 22);
			this.NUPvowelPercent.TabIndex = 6;
			this.NUPvowelPercent.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
			// 
			// CBincludeSyllab
			// 
			this.CBincludeSyllab.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.CBincludeSyllab.Location = new System.Drawing.Point(6, 76);
			this.CBincludeSyllab.Name = "CBincludeSyllab";
			this.CBincludeSyllab.Size = new System.Drawing.Size(272, 17);
			this.CBincludeSyllab.TabIndex = 2;
			this.CBincludeSyllab.Text = "Inclure des syllabes dans la composition du mot";
			this.CBincludeSyllab.UseVisualStyleBackColor = true;
			this.CBincludeSyllab.CheckedChanged += new System.EventHandler(this.CBincludeSyllab_CheckedChanged);
			// 
			// LsyllablePercent
			// 
			this.LsyllablePercent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LsyllablePercent.Enabled = false;
			this.LsyllablePercent.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.LsyllablePercent.Image = global::WordGen.Properties.Resources.StatusAnnotations_Help_and_inconclusive_16xLG_color;
			this.LsyllablePercent.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.LsyllablePercent.Location = new System.Drawing.Point(25, 96);
			this.LsyllablePercent.Name = "LsyllablePercent";
			this.LsyllablePercent.Size = new System.Drawing.Size(270, 16);
			this.LsyllablePercent.TabIndex = 4;
			this.LsyllablePercent.Text = "Probabilité (%)";
			this.LsyllablePercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// NUPsyllablePercent
			// 
			this.NUPsyllablePercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.NUPsyllablePercent.Enabled = false;
			this.NUPsyllablePercent.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.NUPsyllablePercent.Location = new System.Drawing.Point(301, 96);
			this.NUPsyllablePercent.Name = "NUPsyllablePercent";
			this.NUPsyllablePercent.Size = new System.Drawing.Size(61, 22);
			this.NUPsyllablePercent.TabIndex = 3;
			// 
			// CBallowBeginSyllable
			// 
			this.CBallowBeginSyllable.AutoSize = true;
			this.CBallowBeginSyllable.Enabled = false;
			this.CBallowBeginSyllable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.CBallowBeginSyllable.Location = new System.Drawing.Point(28, 124);
			this.CBallowBeginSyllable.Name = "CBallowBeginSyllable";
			this.CBallowBeginSyllable.Size = new System.Drawing.Size(269, 17);
			this.CBallowBeginSyllable.TabIndex = 7;
			this.CBallowBeginSyllable.Text = "Autoriser que le mot commence par une syllabe";
			this.CBallowBeginSyllable.UseVisualStyleBackColor = true;
			this.CBallowBeginSyllable.CheckedChanged += new System.EventHandler(this.CBallowBeginSyllable_CheckedChanged);
			// 
			// LbeginSyllablePercent
			// 
			this.LbeginSyllablePercent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LbeginSyllablePercent.Enabled = false;
			this.LbeginSyllablePercent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.LbeginSyllablePercent.Image = global::WordGen.Properties.Resources.StatusAnnotations_Help_and_inconclusive_16xLG_color;
			this.LbeginSyllablePercent.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.LbeginSyllablePercent.Location = new System.Drawing.Point(45, 144);
			this.LbeginSyllablePercent.Name = "LbeginSyllablePercent";
			this.LbeginSyllablePercent.Size = new System.Drawing.Size(250, 16);
			this.LbeginSyllablePercent.TabIndex = 8;
			this.LbeginSyllablePercent.Text = "Probabilité (%)";
			this.LbeginSyllablePercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// NUPbeginSyllablePercent
			// 
			this.NUPbeginSyllablePercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.NUPbeginSyllablePercent.Enabled = false;
			this.NUPbeginSyllablePercent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.NUPbeginSyllablePercent.Location = new System.Drawing.Point(301, 144);
			this.NUPbeginSyllablePercent.Name = "NUPbeginSyllablePercent";
			this.NUPbeginSyllablePercent.Size = new System.Drawing.Size(61, 22);
			this.NUPbeginSyllablePercent.TabIndex = 3;
			// 
			// TThelp
			// 
			this.TThelp.AutoPopDelay = 60000;
			this.TThelp.InitialDelay = 500;
			this.TThelp.ReshowDelay = 100;
			this.TThelp.ShowAlways = true;
			this.TThelp.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			// 
			// EPmain
			// 
			this.EPmain.ContainerControl = this;
			this.EPmain.Icon = ((System.Drawing.Icon)(resources.GetObject("EPmain.Icon")));
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(412, 402);
			this.Controls.Add(this.RBwordsLenght);
			this.Controls.Add(this.RBcustom);
			this.Controls.Add(this.GBlenght);
			this.Controls.Add(this.GBsyntax);
			this.Controls.Add(this.PCommand);
			this.Controls.Add(this.BchooseSyllabaire);
			this.Controls.Add(this.Lsyllabaire);
			this.Controls.Add(this.DDsyllbary);
			this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimumSize = new System.Drawing.Size(389, 376);
			this.Name = "MainForm";
			this.Text = "Générateur de mots";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.Move += new System.EventHandler(this.MainForm_Move);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			((System.ComponentModel.ISupportInitialize)(this.NUPwordsLenght)).EndInit();
			this.PCommand.ResumeLayout(false);
			this.PCommand.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.NUPnbWords)).EndInit();
			this.GBsyntax.ResumeLayout(false);
			this.GBsyntax.PerformLayout();
			this.GBlenght.ResumeLayout(false);
			this.GBlenght.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.NUPvowelPercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.NUPsyllablePercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.NUPbeginSyllablePercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BSsyllabary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.EPmain)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox DDsyllbary;
        private System.Windows.Forms.Label Lsyllabaire;
        private System.Windows.Forms.Button BchooseSyllabaire;
        private System.Windows.Forms.RadioButton RBwordsLenght;
        private System.Windows.Forms.NumericUpDown NUPwordsLenght;
        private System.Windows.Forms.RadioButton RBcustom;
        private System.Windows.Forms.TextBox TBwordStructure;
        private System.Windows.Forms.Button Bgenerate;
        private System.Windows.Forms.Panel PCommand;
		private System.Windows.Forms.NumericUpDown NUPnbWords;
		private System.Windows.Forms.Label LnbWords;
		private System.Windows.Forms.GroupBox GBsyntax;
		private System.Windows.Forms.GroupBox GBlenght;
		private System.Windows.Forms.ToolTip TThelp;
		private System.Windows.Forms.Label LsyntaxHelp;
		private System.Windows.Forms.Label LnbChars;
		private System.Windows.Forms.BindingSource BSsyllabary;
		private System.Windows.Forms.CheckBox CBincludeSyllab;
		private System.Windows.Forms.ErrorProvider EPmain;
		private System.Windows.Forms.Label LquickGuide;
		private System.Windows.Forms.Label LsyllablePercent;
		private System.Windows.Forms.NumericUpDown NUPsyllablePercent;
		private System.Windows.Forms.Label LvowelPercent;
		private System.Windows.Forms.NumericUpDown NUPvowelPercent;
		private System.Windows.Forms.CheckBox CBallowBeginSyllable;
		private System.Windows.Forms.Label LbeginSyllablePercent;
		private System.Windows.Forms.NumericUpDown NUPbeginSyllablePercent;
	}
}

