﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordGen.Model {
	class GeneratorSettings {


		public decimal _vowelBeginProba { get; set; }
		public decimal _syllableProba { get; set; }
		public decimal _syllableBeginProba { get; set; }

		public GeneratorSettings() {
			_vowelBeginProba = 50;
			_syllableProba = -1; //10;
			_syllableBeginProba = -1; // 0;
		}

		public GeneratorSettings(UInt16 vowelBeginProba, UInt16 syllableProba, UInt16 syllableBeginProba) {
			_vowelBeginProba = vowelBeginProba;
			_syllableBeginProba = syllableBeginProba;
			_syllableProba = syllableProba;
		}

	}
}
