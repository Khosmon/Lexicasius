﻿using System;
using System.Runtime.Serialization;
using WordGen.Model.Exceptions;
using WordGen.View.LocalizationString;

namespace WordGen.Model.Exceptions {
	/// <summary>
	/// Aucune consonne n'as été trouvée. Ajoutez des consonnes à votre syllabaire, retirez les consonnes de la syntaxe de votre mot (caractère 'c') ou paramétrez votre syllabaire pour ne pas insérer automatiquement une consonne entre une voyelle et une syllabe.
	/// </summary>
	[Serializable]
	internal class EmptyConsonnantListException : GeneratorException {
		 
		public EmptyConsonnantListException() : base(Messages.GeneratorError_EmptyConsonnantListException) {
		}

		public EmptyConsonnantListException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, Messages.GeneratorError_EmptyConsonnantListException) {
		}

		public EmptyConsonnantListException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, message) {
		}

		public EmptyConsonnantListException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message, Exception innerException) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, message, innerException) {
		}

	}
}