﻿using System;
using System.Runtime.Serialization;
using WordGen.Model.Exceptions;
using WordGen.View.LocalizationString;

namespace WordGen.Model.Exceptions {
	/// <summary>
	/// Aucune syllabe n'as été trouvée. Ajoutez des syllabe à votre syllabaire ou retirez les syllabes de la syntaxe de votre mot (caractère 's').
	/// </summary>
	[Serializable]
	internal class EmptySyllableListException : GeneratorException {
		public EmptySyllableListException() : base(Messages.GeneratorError_EmptySyllableListException) {
		}

		public EmptySyllableListException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, Messages.GeneratorError_EmptySyllableListException) {
		}

		public EmptySyllableListException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, message) {
		}

		protected EmptySyllableListException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message, Exception innerException) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, message, innerException) {
		}
	}
}