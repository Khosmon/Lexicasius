﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using WordGen.Model;

namespace WordGen {
	public static class SyllabaryWrapper {

		/// <summary>
		/// La liste des Syllabaire géré par l'application.
		/// </summary>
		private static List<Syllabary> _Sillabaries = new List<Syllabary>();

		internal static int count() {
			return _Sillabaries.Count;
		}

		/// <summary>
		/// Vérifie que le nom fournit n'existe pas déjà dans la liste. Propose un nom alternatif également vérifié. La fonction est récustive à chaque modifications et garantie qu'il n'est pas possible de posséder le même nom dans la liste.
		/// </summary>
		/// <param name="Name">Le nom a vérifier.</param>
		/// <param name="Original">Si le nom est déjà utilisé, le nom nom original</param>
		/// <param name="Version">Le numéro utilisé pour différentier le nom a vérifier d'un nom déjà existant</param>
		/// <returns></returns>
		private static string CheckName(string Name, string Original = null, uint Version = 1) {
			// Nous comparons le nom avec chaque item existant dans la liste.
			foreach (Syllabary item in _Sillabaries ) {
				// Si l'un des item à un nom identique au nom spécifié ...
				if (item.title.Equals(Name)) {

					// ... et si la fonction n'as pas déjà suggéré un nouveau nom ...
					if (Original == null) {
						// on sauvegarde le nom original pour les appels ultérieurs.
						Original = Name;
						// On suggère un nouveau nom basé sur l'ancien et une version.
						Name = Name + " (" + Version + ")";

					} else {
						// Sinon...
						// ...On reprends le nom original  et on utilise la version (qui a été incrémenté dans la méthode appelante)
						Name = Original + " (" + Version + ")";
					}

					// On vérifie que le nom suggéré n'existe pas déjà, en fournissant l'original et une version supérieure.
					Name = CheckName(Name, Original, ++Version);
				}
			}
			return Name;
		}

		/// <summary>
		/// Enregistre dans le répertoire AppData les syllabaires de l'application sous forme de fichier XML. Les fichiers existant sont remplacé.
		/// </summary>
		public static void SaveToDrive() {
			SyllabaryIoManager siom = new SyllabaryIoManager();
			siom.saveAllSyllabaries(_Sillabaries, SyllabaryIoManager.UserAppData);
		}

		/// <summary>
		/// Récupère depuis le répertoire AppData les fichier de syllabaires existants.
		/// </summary>
		public static void LoadFromDrive() {
			SyllabaryIoManager siom = new SyllabaryIoManager();
			AddRange(siom.loadAllSyllabaries(SyllabaryIoManager.UserAppData));
		}

		/// <summary>
		/// Obtient le syllabaire de la liste des syllabaires géré par le wrapper, spécifié par son titre. Lève une exception si aucun syllabaire avec le titre spécifié n'est trouvé.
		/// </summary>
		public static Syllabary Get(string thisSyllabary) {
			foreach (Syllabary item in _Sillabaries) {
				if (item.title.Equals(thisSyllabary)) {
					return item;
				}
			}
			throw new KeyNotFoundException();
		}

		/// <summary>
		/// Obtient la liste complète des syllabaires géré par le wrapper.
		/// </summary>
		public static List<Syllabary> GetAll() {
			return _Sillabaries;
		}

		/// <summary>
		/// Ajoute le syllabaire spécifié à la liste des syllabaires géré par le wrapper.
		/// </summary>
		/// <param name="aSyllabary">Le syllabaire qui sera ajouté.</param>
		public static void Add(Syllabary aSyllabary) {
			aSyllabary.title = CheckName(aSyllabary.title);
			_Sillabaries.Add(aSyllabary);
		}

		/// <summary>
		/// Remplace un syllabaire de la liste des syllabaires géré par le wrapper, spécifié par son titre, par un nouveau syllabaire spécifié. Lève une exception si aucun syllabaire avec le titre spécifié n'est trouvé.
		/// </summary>
		/// <param name="thisSyllabaryTitle">Le titre du syllabaire à remplacer.</param>
		/// <param name="withThisSyllabary">Le syllabaire qui sera gardé.</param>
		public static void Replace(string thisSyllabaryTitle, Syllabary withThisSyllabary) {
			foreach (Syllabary item in _Sillabaries) {
				if (item.title.Equals(thisSyllabaryTitle)) {
					_Sillabaries.Remove(item);
					withThisSyllabary.title = CheckName(withThisSyllabary.title);
          _Sillabaries.Add(withThisSyllabary);
					break;
				}
			}
		}

		/// <summary>
		/// Supprime un syllabaire de la liste des syllabaires géré par le wrapper, spécifié par son titre. Lève une exception si aucun syllabaire avec le titre spécifié n'est trouvé.
		/// </summary>
		/// <param name="thisSyllabaryTitle">Le titre du syllabaire à supprimer.</param>
		public static void Delete(string thisSyllabaryTitle) {
			foreach (Syllabary item in _Sillabaries) {
				if (item.title.Equals(thisSyllabaryTitle)) {
					_Sillabaries.Remove(item);
					break;
				}
			}
		}

		/// <summary>
		/// Ajoute la liste des syllabaies spécifiés à la liste des syllabires géré par le wrapper.
		/// </summary>
		public static void AddRange(IEnumerable<Syllabary> thesesSyllabaries) {
			_Sillabaries.AddRange(thesesSyllabaries);
		}
	}
}