﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordGen.Model;

namespace WordGen {
	static class Program {

		/// <summary>
		/// Point d'entrée principal de l'application.
		/// </summary>w
		[STAThread]
		static void Main() {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			SyllabaryWrapper.LoadFromDrive();
			Application.Run(new View.MainForm());
		}
	}
}
