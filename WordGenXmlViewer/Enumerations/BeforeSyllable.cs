﻿using System;

[Flags]
public enum BeforeSyllable : int {
	AsIs,
	AutoSelect,
	AutoInsert
}