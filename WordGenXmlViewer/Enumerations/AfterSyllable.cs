﻿using System;

[Flags]
public enum AfterSyllable : int {
	AsIs,
	AutoInsert
}