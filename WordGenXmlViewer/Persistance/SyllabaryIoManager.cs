﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;


	/// <summary>
	/// Représente un gestionnaire de sauvegarde et de lecture d'un ou plusieurs Syllabaires pour l'application en exposant des méthodes permettant de gérer des fichiers XML représentant des Syllabaires.
	/// </summary>
	public class SyllabaryIoManager : FileIoManager {

		/// <summary>
		/// Représente le dossier local applicatif et utilisateur.
		/// </summary>
		public String UserAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);


		/// <summary>
		/// Sauvegarde le Syllabaire dans un fichier a l'emplacement indiqué. Utilise le titre comme nom de fichier.
		/// </summary>
		/// <param name="thisSyllabary">Le sillabraire à sauvegarder dans un fichier XML</param>
		/// <param name="thisFolder">L'emplacement du dossier dans lequel ce Syllabaire sera créé. Si l'URI est un fichier, son nom est utilisé à la place du titre du Syllabaire.</param>
		public void saveSyllabary(Syllabary thisSyllabary, Uri thisFolder) {
			XmlSerializer xs = new XmlSerializer(typeof(Syllabary));
			using (StreamWriter wr = new StreamWriter(thisFolder.AbsolutePath)) {
				xs.Serialize(wr, thisSyllabary);
			}
		}

	/// <summary>
	/// Charge le Syllabaire depuis le fichier précisé
	/// </summary>
	/// <param name="thisSillabaryFile">L'emplacement du fichier XML représentant un Syllabaire</param>
	public Syllabary loadSyllabary(Uri thisSillabaryFile) {
		XmlSerializer xs = new XmlSerializer(typeof(Syllabary));
		using (StreamReader sr = new StreamReader(thisSillabaryFile.AbsolutePath)) {
			Syllabary mySylla = xs.Deserialize(sr) as Syllabary;
			return mySylla;
		}
	}

		/// <summary>
		/// Charge l'ensemble des fichier Syllabaire depuis le dossier précisé
		/// </summary>
		/// <param name="thisFolder">L'emplacement du dossier dans lequel les Syllabaires seront chargés.</param>
		public List<Syllabary> loadAllSyllabaries(Uri thisFolder) {
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// Sauvegarde l'ensemble des objets Syllabaires spécifiés à l'emplacement spécifié.
		/// </summary>
		/// <param name="thosesSillabaries">La liste des Syllabaires à sauvegarder dans des fichiers XML.</param>
		/// <param name="thisFolder">L'emplacement du dossier dans lequel les Syllabaires seront créés.</param>
		public void saveAllSyllabaries(List<Syllabary> thosesSillabaries, Uri thisFolder) {
			throw new System.NotImplementedException();
		}
	}
