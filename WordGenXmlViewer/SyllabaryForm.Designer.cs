﻿
	partial class SyllabaryForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SyllabaryForm));
			this.LsyllabaryName = new System.Windows.Forms.Label();
			this.TBsyllabaryName = new System.Windows.Forms.TextBox();
			this.TBconsonants = new System.Windows.Forms.TextBox();
			this.TBvowels = new System.Windows.Forms.TextBox();
			this.TBsyllables = new System.Windows.Forms.TextBox();
			this.Cliste = new System.Windows.Forms.TableLayoutPanel();
			this.Lsyllable = new System.Windows.Forms.Label();
			this.Lconsonant = new System.Windows.Forms.Label();
			this.Lvowel = new System.Windows.Forms.Label();
			this.DDbeginSyllables = new System.Windows.Forms.ComboBox();
			this.DDendSyllables = new System.Windows.Forms.ComboBox();
			this.EPsyllabary = new System.Windows.Forms.ErrorProvider(this.components);
			this.LbeginSyllable = new System.Windows.Forms.Label();
			this.LendSyllable = new System.Windows.Forms.Label();
			this.TTsyllablesHelp = new System.Windows.Forms.ToolTip(this.components);
			this.LhelpEndSyllable = new System.Windows.Forms.Label();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
			this.Cliste.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.EPsyllabary)).BeginInit();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// LsyllabaryName
			// 
			this.LsyllabaryName.AutoSize = true;
			this.LsyllabaryName.Location = new System.Drawing.Point(12, 31);
			this.LsyllabaryName.Name = "LsyllabaryName";
			this.LsyllabaryName.Size = new System.Drawing.Size(100, 13);
			this.LsyllabaryName.TabIndex = 0;
			this.LsyllabaryName.Text = "Nom du Syllabaire";
			// 
			// TBsyllabaryName
			// 
			this.TBsyllabaryName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TBsyllabaryName.Location = new System.Drawing.Point(118, 28);
			this.TBsyllabaryName.MaxLength = 50;
			this.TBsyllabaryName.Name = "TBsyllabaryName";
			this.TBsyllabaryName.Size = new System.Drawing.Size(228, 22);
			this.TBsyllabaryName.TabIndex = 1;
			this.TBsyllabaryName.TextChanged += new System.EventHandler(this.TBsyllabaryName_TextChanged);
			this.TBsyllabaryName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// TBconsonants
			// 
			this.TBconsonants.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TBconsonants.Location = new System.Drawing.Point(3, 23);
			this.TBconsonants.Multiline = true;
			this.TBconsonants.Name = "TBconsonants";
			this.TBconsonants.Size = new System.Drawing.Size(113, 213);
			this.TBconsonants.TabIndex = 0;
			this.TBconsonants.WordWrap = false;
			this.TBconsonants.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// TBvowels
			// 
			this.TBvowels.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TBvowels.Location = new System.Drawing.Point(122, 23);
			this.TBvowels.Multiline = true;
			this.TBvowels.Name = "TBvowels";
			this.TBvowels.Size = new System.Drawing.Size(114, 213);
			this.TBvowels.TabIndex = 1;
			this.TBvowels.WordWrap = false;
			this.TBvowels.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// TBsyllables
			// 
			this.TBsyllables.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TBsyllables.Location = new System.Drawing.Point(242, 23);
			this.TBsyllables.Multiline = true;
			this.TBsyllables.Name = "TBsyllables";
			this.TBsyllables.Size = new System.Drawing.Size(115, 213);
			this.TBsyllables.TabIndex = 2;
			this.TBsyllables.WordWrap = false;
			this.TBsyllables.TextChanged += new System.EventHandler(this.TBsyllables_TextChanged);
			this.TBsyllables.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// Cliste
			// 
			this.Cliste.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Cliste.ColumnCount = 3;
			this.Cliste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
			this.Cliste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.Cliste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.Cliste.Controls.Add(this.Lsyllable, 2, 0);
			this.Cliste.Controls.Add(this.Lconsonant, 1, 0);
			this.Cliste.Controls.Add(this.Lvowel, 0, 0);
			this.Cliste.Controls.Add(this.TBconsonants, 0, 1);
			this.Cliste.Controls.Add(this.TBsyllables, 2, 1);
			this.Cliste.Controls.Add(this.TBvowels, 1, 1);
			this.Cliste.Location = new System.Drawing.Point(12, 56);
			this.Cliste.Name = "Cliste";
			this.Cliste.RowCount = 2;
			this.Cliste.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.Cliste.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Cliste.Size = new System.Drawing.Size(360, 239);
			this.Cliste.TabIndex = 4;
			// 
			// Lsyllable
			// 
			this.Lsyllable.AutoSize = true;
			this.Lsyllable.Dock = System.Windows.Forms.DockStyle.Left;
			this.Lsyllable.Location = new System.Drawing.Point(242, 0);
			this.Lsyllable.Name = "Lsyllable";
			this.Lsyllable.Size = new System.Drawing.Size(46, 20);
			this.Lsyllable.TabIndex = 6;
			this.Lsyllable.Text = "Syllable";
			this.Lsyllable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Lconsonant
			// 
			this.Lconsonant.AutoSize = true;
			this.Lconsonant.Dock = System.Windows.Forms.DockStyle.Left;
			this.Lconsonant.Location = new System.Drawing.Point(122, 0);
			this.Lconsonant.Name = "Lconsonant";
			this.Lconsonant.Size = new System.Drawing.Size(60, 20);
			this.Lconsonant.TabIndex = 5;
			this.Lconsonant.Text = "Consonne";
			this.Lconsonant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Lvowel
			// 
			this.Lvowel.AutoSize = true;
			this.Lvowel.Dock = System.Windows.Forms.DockStyle.Left;
			this.Lvowel.Location = new System.Drawing.Point(3, 0);
			this.Lvowel.Name = "Lvowel";
			this.Lvowel.Size = new System.Drawing.Size(43, 20);
			this.Lvowel.TabIndex = 4;
			this.Lvowel.Text = "Voyelle";
			this.Lvowel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// DDbeginSyllables
			// 
			this.DDbeginSyllables.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DDbeginSyllables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.DDbeginSyllables.Enabled = false;
			this.DDbeginSyllables.FormattingEnabled = true;
			this.DDbeginSyllables.Items.AddRange(new object[] {
            "Ne pas adapter",
            "Choisir automatiquement",
            "Intercaller automatiquement"});
			this.DDbeginSyllables.Location = new System.Drawing.Point(222, 301);
			this.DDbeginSyllables.Name = "DDbeginSyllables";
			this.DDbeginSyllables.Size = new System.Drawing.Size(150, 21);
			this.DDbeginSyllables.TabIndex = 6;
			this.DDbeginSyllables.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// DDendSyllables
			// 
			this.DDendSyllables.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DDendSyllables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.DDendSyllables.Enabled = false;
			this.DDendSyllables.FormattingEnabled = true;
			this.DDendSyllables.Items.AddRange(new object[] {
            "Ne pas adapter",
            "Intercaler automatiquement"});
			this.DDendSyllables.Location = new System.Drawing.Point(222, 328);
			this.DDendSyllables.Name = "DDendSyllables";
			this.DDendSyllables.Size = new System.Drawing.Size(150, 21);
			this.DDendSyllables.TabIndex = 8;
			this.DDendSyllables.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// EPsyllabary
			// 
			this.EPsyllabary.ContainerControl = this;
			this.EPsyllabary.Icon = ((System.Drawing.Icon)(resources.GetObject("EPsyllabary.Icon")));
			// 
			// LbeginSyllable
			// 
			this.LbeginSyllable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.LbeginSyllable.AutoSize = true;
			this.LbeginSyllable.Location = new System.Drawing.Point(12, 304);
			this.LbeginSyllable.Name = "LbeginSyllable";
			this.LbeginSyllable.Size = new System.Drawing.Size(97, 13);
			this.LbeginSyllable.TabIndex = 5;
			this.LbeginSyllable.Text = "Avant une syllabe";
			// 
			// LendSyllable
			// 
			this.LendSyllable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.LendSyllable.AutoSize = true;
			this.LendSyllable.Location = new System.Drawing.Point(12, 331);
			this.LendSyllable.Name = "LendSyllable";
			this.LendSyllable.Size = new System.Drawing.Size(97, 13);
			this.LendSyllable.TabIndex = 7;
			this.LendSyllable.Text = "Après une syllabe";
			// 
			// TTsyllablesHelp
			// 
			this.TTsyllablesHelp.AutomaticDelay = 0;
			this.TTsyllablesHelp.AutoPopDelay = 50000;
			this.TTsyllablesHelp.InitialDelay = 500;
			this.TTsyllablesHelp.ReshowDelay = 100;
			this.TTsyllablesHelp.ShowAlways = true;
			this.TTsyllablesHelp.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			// 
			// LhelpEndSyllable
			// 
			this.LhelpEndSyllable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.LhelpEndSyllable.Location = new System.Drawing.Point(352, 292);
			this.LhelpEndSyllable.Name = "LhelpEndSyllable";
			this.LhelpEndSyllable.Size = new System.Drawing.Size(16, 16);
			this.LhelpEndSyllable.TabIndex = 15;
			// 
			// toolStrip1
			// 
			this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton3,
            this.toolStripSeparator1,
            this.toolStripButton2});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip1.Size = new System.Drawing.Size(384, 25);
			this.toolStrip1.TabIndex = 16;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// toolStripButton1
			// 
			this.toolStripButton1.Image = global::WordGenXmlViewer.Properties.Resources.Open_6529;
			this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton1.Name = "toolStripButton1";
			this.toolStripButton1.Size = new System.Drawing.Size(69, 22);
			this.toolStripButton1.Text = "Ouvrir...";
			this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
			// 
			// toolStripButton3
			// 
			this.toolStripButton3.Image = global::WordGenXmlViewer.Properties.Resources.Close_6519_32;
			this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Black;
			this.toolStripButton3.Name = "toolStripButton3";
			this.toolStripButton3.Size = new System.Drawing.Size(64, 22);
			this.toolStripButton3.Text = "Fermer";
			this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripButton2
			// 
			this.toolStripButton2.Image = global::WordGenXmlViewer.Properties.Resources.save_16xLG;
			this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton2.Name = "toolStripButton2";
			this.toolStripButton2.Size = new System.Drawing.Size(92, 22);
			this.toolStripButton2.Text = "Enregistrer...";
			this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
			// 
			// SyllabaryForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 361);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.DDbeginSyllables);
			this.Controls.Add(this.LbeginSyllable);
			this.Controls.Add(this.DDendSyllables);
			this.Controls.Add(this.LendSyllable);
			this.Controls.Add(this.LhelpEndSyllable);
			this.Controls.Add(this.Cliste);
			this.Controls.Add(this.TBsyllabaryName);
			this.Controls.Add(this.LsyllabaryName);
			this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
			this.MinimumSize = new System.Drawing.Size(400, 400);
			this.Name = "SyllabaryForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Syllabaire";
			this.Load += new System.EventHandler(this.SyllabaryForm_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			this.Cliste.ResumeLayout(false);
			this.Cliste.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.EPsyllabary)).EndInit();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LsyllabaryName;
		private System.Windows.Forms.TextBox TBsyllabaryName;
		private System.Windows.Forms.TextBox TBconsonants;
		private System.Windows.Forms.TextBox TBvowels;
		private System.Windows.Forms.TextBox TBsyllables;
		private System.Windows.Forms.TableLayoutPanel Cliste;
		private System.Windows.Forms.ComboBox DDbeginSyllables;
		private System.Windows.Forms.ComboBox DDendSyllables;
		private System.Windows.Forms.ErrorProvider EPsyllabary;
		private System.Windows.Forms.ToolTip TTsyllablesHelp;
		private System.Windows.Forms.Label Lsyllable;
		private System.Windows.Forms.Label Lconsonant;
		private System.Windows.Forms.Label Lvowel;
		private System.Windows.Forms.Label LendSyllable;
		private System.Windows.Forms.Label LbeginSyllable;
		private System.Windows.Forms.Label LhelpEndSyllable;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton toolStripButton1;
		private System.Windows.Forms.ToolStripButton toolStripButton3;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton toolStripButton2;

		public object getSyllabaire { get; internal set; }

	}